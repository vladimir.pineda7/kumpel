# Kumpel

<div align="center">
	<img src="https://img.shields.io/badge/npm-6.14.4-blue"  alt="npm badge">
	<img src="https://img.shields.io/badge/license-GPL--3.0-blue"  alt="license badge">
	<img src="https://img.shields.io/badge/react-16.13.1-blue"  alt="react bagde">
	<img src="https://img.shields.io/badge/version-1.0.0-green"  alt="version bagde">
</div>

<div align="center">
 <a href="https://github.com/Esteban-Ladino">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/Esteban-Ladino?label=Esteban%20Ladino&style=social">
    </a>
    <a href="https://github.com/emmaisworking">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/emmaisworking?label=emmaIsWorking&style=social">
    </a>
    <a href="https://github.com/AndresCampuzano">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/AndresCampuzano?label=Andres%20Campuzano&style=social">
    </a>
</div>

<div align="center">
    <a href="https://twitter.com/emmaisworking">
      <img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=EmmaIsWorking&style=social&url=https%3A%2F%2Ftwitter.com%2Femmaisworking">
    </a>
    <a href="https://twitter.com/AndresCampuzan0">
    <img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=AndresCampuzan0&style=social&url=https%3A%2F%2Ftwitter.com%2AndresCampuzan0">
    </a>
</div>


Kumpel frontend
The project consists of generating a web application where the user can open a request or offer hosting.

## Demo

If you want to see the demo of this project deployed, you can visit it here

## How to clone
You can clone the repository

    $ git clone git@gitlab.com:teamspartans/rommie/frontend/kumpel.git
    
## Installation
To install this project just type

    $ npm install

To excecute type

    $ npm run start

To build type

    $ npm run build

## Preview



## How to contribute

You can create a Merge Request to the project

## License

GPL-3.0